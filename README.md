

# Setup
1. Clean Ubuntu 16.04
1. GPU ONLY: Install nvidia drivers (example for GTX 970). [Find latest driver version](http://www.nvidia.com/Download/index.aspx).
    ```bash
    sudo apt-get update && sudo apt-get install -y software-properties-common
    sudo add-apt-repository -y ppa:graphics-drivers/ppa && sudo apt-get update
    // Update to latest version
    sudo apt-get install -y nvidia-384
    sudo reboot
    ```
1. Install docker and docker-compose
    ```bash
    sudo bash docker_with_compose.sh
    ```
1. GPU ONLY: Install nvidia-docker
    ```bash
    sudo bash nvidia-docker.sh
    ```
1. SSH tunnel with VNC and noVNC (browser)
    ```
    ssh -L 6080:localhost:6080 -L 5900:localhost:5900 user@remote.machine.com
    ```
1. Run image as daemon
    ```
    docker run --restart=always -p 127.0.0.1:6080:6080 -p 127.0.0.1:5900:5900 -d --user=$(id -u):$(id -g) --name=my_pytorch_workspace -v $(pwd)/workspace:/home/ubuntu/shared -it nanotrix/pytorch-workspace:1.0.0-cu90 start_vnc
    ```

## How to connect (VNC setup):
Open browser  http://localhost:6080/vnc.html?resize=downscale&autoconnect=1&password=vnc

Or Install vnc-viewer https://www.realvnc.com/en/connect/download/viewer/ and connect to localhost:5900

# Build
1. docker build -t nanotrix/pytorch-workspace:1.0.0-cu90 .
1. docker push nanotrix/pytorch-workspace:1.0.0-cu90

